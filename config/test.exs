import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :phoenix_hello, PhoenixHelloWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "IAeLN0F/ZJ0+738tRCeuzoeDXGBM4tUKxuLZtsiDRW8CaVftusTAPRWj/ROO4Lo7",
  server: false

# In test we don't send emails.
config :phoenix_hello, PhoenixHello.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

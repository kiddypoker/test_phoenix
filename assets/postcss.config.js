module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-nested': {},
    'postcss-animation': {},
    'cssnano': {preset: 'default'},
    tailwindcss: {},
    autoprefixer: {},
  }
}